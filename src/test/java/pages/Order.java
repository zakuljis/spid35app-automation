package pages;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;

import utilities.Page;

public final class Order extends Page {
    public Order(AppiumDriver driver) {
        super(driver, AppiumBy.id(appPackage + ".orders:id/order_detail_nav_host"));
        WaitToLoad();
    }
}