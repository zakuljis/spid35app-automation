package pages;

import java.io.FileNotFoundException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.HowToUseLocators;
import io.appium.java_client.pagefactory.LocatorGroupStrategy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import utilities.Page;
import utilities.Product;
import utilities.Connection;
import utilities.Generics;

public final class Home extends Page {
    public enum SellerError {
        OUTOFHOURS,
        OUTOFCOVERAGE;
    }

    // Location
    @AndroidFindBy(id = appPackage + ":id/search_src_text")
    @iOSXCUITFindBy(accessibility = "magnifyingGlass")
    private WebElement searchAddrBar;
    @AndroidFindBy(xpath =
        "//*[@resource-id='" + appPackage + ".location:id/rvLocations']" + 
            "/android.widget.LinearLayout" + 
                "/*[@resource-id='" + appPackage + ".location:id/layoutItem']")
    private WebElement lastUsedAddr;
    @AndroidFindBy(id = appPackage + ".location:id/rvLocationResults")
    private WebElement searchAddrResults;
    @AndroidFindBy(id = appPackage + ".location:id/btnConfirm")
    private WebElement setAddrBtn;
    @AndroidFindBy(id = appPackage + ":id/seller_error_tv")
    private WebElement sellerError;
    // Home
    @AndroidFindBy(id = appPackage + ".navigationmenu:id/bottom_item_categories")
    private WebElement shortcutCategoriesBtn;
    @AndroidFindBy(xpath =
        "//*[@resource-id='" + appPackage + ".home:id/cartIcon' or @resource-id='" + appPackage + ":id/cartIcon']"
        +   "/*[@resource-id='" + appPackage + ":id/container']"
            +   "/android.view.ViewGroup/*[@resource-id='" + appPackage + ":id/tvCount']"
    )
    private WebElement cartSize;
    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @AndroidFindBy(id = appPackage + ".home:id/cartIcon") @AndroidFindBy(id = appPackage + ":id/cartIcon")
    private WebElement cartDetail;

    // TODO: Separar la pestaña de location del home
    public static Home createInstance(Connection connection) throws Exception {
        if (connection.isAndroid()) {
            return new Home(connection,
                AppiumBy.id(appPackage + ".location:id/location_nav_host"),
                AppiumBy.xpath("//*[@resource-id='" + appPackage + ".navigationmenu:id/menuhome_nav_host']")
            );
        }
        else if (connection.isIOS()) {
            return new Home(connection,
                AppiumBy.accessibilityId("Dirección de entrega")
            );
        }
        else {
            throw new Exception("Unimplemented OS");
        }
    }
    
    private Home(Connection connection, By... root) {
        super(connection, root);
        WaitToLoad();
    }

    public WebElement searchAddress(String address) throws Exception {
        searchAddrBar.click();
        midWait.until(ExpectedConditions.elementToBeClickable(searchAddrBar));
        searchAddrBar.sendKeys(address);
        Thread.sleep(5000);
        
        try {
            if (connection.isAndroid()) {
                return searchAddrResults.findElement(
                    AppiumBy.xpath(
                        ".//android.widget.LinearLayout[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]"
                    )
                );
            }
            else if (connection.isIOS()) {
                return driver.findElement(
                    AppiumBy.xpath(
                        "//XCUIElementTypeStaticText[@name='" + address + "']"
                    )
                );
            }
            else {
                throw new Exception("Unimplemented OS");
            }
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public void setAddress(String address) throws Exception {
        searchAddress(address).click();
        midWait.until(
            ExpectedConditions.elementToBeClickable(setAddrBtn)
        );
        setAddrBtn.click();
        WaitToLoad();
    }

    public void useOrSetAddress(String address) throws Exception {
        try {
            lastUsedAddr.click();
            WaitToLoad();
        } catch (NoSuchElementException e) {
            setAddress(address);
        }
    }

    public Boolean checkCoverage() {
        try {
            midWait.until(
                ExpectedConditions.visibilityOf(sellerError)
            );
            return false;
        } catch (TimeoutException e) {
            return true;
        }
    }

    public String getSellerError() throws Exception {
        midWait.until(ExpectedConditions.visibilityOf(sellerError));
        return Generics.getNormalizedText(sellerError).replaceAll("\\s+", " ");
    }

    // TODO: Este método está hardcodeado, en realidad debería usar getProduct("Home"),
    // pero por una actualización de la app ese método está roto
    public void addProductToCart() throws Exception {
        WebElement product = longWait.until(
            ExpectedConditions.visibilityOfElementLocated(
                AppiumBy.androidUIAutomator(
                    "new UiScrollable(new UiSelector().scrollable(true))" +
                    ".scrollIntoView(new UiSelector().resourceId(\"" + appPackage + ":id/cvOnlyButton\"))"
                )
            )
        );
        int initCartSize = getCartSize();
        System.out.println(initCartSize);
        product.click();
        if (!isCartSize(initCartSize + 1)) {
            // TODO: Escribir esta excepción si en carro no se actualiza al agregar un producto
            throw new Exception();
        }
    }

    // Este método no funciona a partir de la versión 1.0.10
    public Product getProduct(String from) throws Exception {
        WebElement container;
        Product product;
        switch (from) {
            case "Home":
                container = longWait.until(
                    ExpectedConditions.visibilityOfElementLocated(
                        AppiumBy.androidUIAutomator(
                            "new UiScrollable(new UiSelector().scrollable(true))" +
                            ".scrollIntoView(new UiSelector().resourceId(\"" + appPackage + ":id/product_bar\"))"
                        )
                    )
                );
                product = new Product(
                    container,
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/ibOnProductOperationRequest']"),
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/ibAddProduct']"),
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/tvQuantitySingle' or @resource-id='" + appPackage + "id/tvQuantityComposed']"),
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/ibDeleteLastProduct']"),
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/ibDeleteProduct']")
                );
                break;
            case "PLP":
                longWait.until(
                    ExpectedConditions.and(
                        ExpectedConditions.elementToBeClickable(shortcutCategoriesBtn),
                        ExpectedConditions.attributeToBe(shortcutCategoriesBtn, "clickable", "true")
                    )
                );
                // Entrar en las categorías
                shortcutCategoriesBtn.click();
                WaitToLoad();
                int numberOfCategories = driver.findElements(AppiumBy.xpath(
                    "//*[@resource-id='" + appPackage + ".categories:id/item_gridlayout_category']"
                )).size();
                
                // Este bucle recorre todas las categorías, hasta encontrar una no vacía
                for (int i = 1; i <= numberOfCategories; i++) {
                    // Entrar a una categoría
                    driver.findElement(AppiumBy.xpath(
                        String.format("//*[@resource-id='%s.categories:id/item_gridlayout_category'][%d]", appPackage, i)
                    )).click();
                    WaitToLoad();

                    try {
                        // Si la categoría no está vacía, entonces nos quedamos
                        if (shortWait.until(ExpectedConditions.invisibilityOfElementLocated(
                            AppiumBy.xpath("//*[@resource-id='" + appPackage + ".categories:id/include_error_empty_list']")
                        ))) {
                            break;
                        }
                    }
                    catch (TimeoutException e) {
                        // Si la categoría está vacía, retrocedemos
                        driver.findElement(AppiumBy.id(appPackage + ":id/ibBackButton")).click();
                        WaitToLoad();
                    }
                }

                container = driver.findElement(AppiumBy.xpath(
                    "//*[@resource-id='" + appPackage + ":id/product_bar']"
                ));
                product = new Product(
                    container,
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/ibOnProductOperationRequest']"),
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/ibAddProduct']"),
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/tvQuantitySingle' or @resource-id='" + appPackage + "id/tvQuantityComposed']"),
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/ibDeleteLastProduct']"),
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ":id/ibDeleteProduct']")
                );
                break;
            case "PDP":
                longWait.until(
                    ExpectedConditions.visibilityOfElementLocated(
                        AppiumBy.androidUIAutomator(
                            "new UiScrollable(new UiSelector().scrollable(true))" +
                            ".scrollIntoView(new UiSelector().resourceId(\"" + appPackage + ":id/containerCardProduct\"))"
                        )
                    )
                ).click();

                // Existe un contenedor para todos estos botones, pero si lo buscaba con
                // AppiumBy.id me retornaba algo nada que ver y buscandolo con xpath no
                // lo encontraba, así que decidí simplemente buscar los botones por id.
                product = new Product(
                    driver,
                    AppiumBy.id(appPackage + ".productdetail:id/btnAdd"),
                    AppiumBy.id(appPackage + ".productdetail:id/lytBtnAdd"),
                    AppiumBy.id(appPackage + ".productdetail:id/tvAddAndRemove"),
                    AppiumBy.id(appPackage + ".productdetail:id/lytBtnDelete"),
                    AppiumBy.id(appPackage + ".productdetail:id/lytBtnDelete")
                );
                break;
            default:
                // TODO: Escribir la excepción de lugar inválido.
                throw new Exception();
        }
        return product;
    }

    private void waitToLoadCart() {
        longWait.until(
            ExpectedConditions.and(
                ExpectedConditions.elementToBeClickable(cartDetail)
                //ExpectedConditions.attributeToBe(cartDetail, "clickable", "true")
            )
        );
    }

    public int getCartSize() {
        waitToLoadCart();
        return Integer.parseInt(Generics.getNormalizedText(cartSize));
    }

    public boolean isCartSize(int number) {
        waitToLoadCart();
        try {
            // No uso midWait o alguna otro waiter puesto que el tiempo de actualización
            // del carrito es de max 5 segs, por lo que es mejor crear un waiter específico
            // y no arriesgarse a un error por el cambio de los otros waiters en un futuro.
            return new WebDriverWait(driver, Duration.ofSeconds(5)).until(
                ExpectedConditions.attributeToBe(cartSize, "text", Integer.toString(number))
            );
        }
        catch (TimeoutException e) {
            return false;
        }  
    }

    public void goToCartDetail() {
        waitToLoadCart();
        cartDetail.click();
    }
}
