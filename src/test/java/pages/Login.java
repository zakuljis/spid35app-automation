package pages;

import java.io.FileNotFoundException;
import java.text.Collator;
import java.time.Duration;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.HowToUseLocators;
import io.appium.java_client.pagefactory.LocatorGroupStrategy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import models.Customer;
import models.User;
import utilities.Connection;
import utilities.Generics;
import utilities.Page;
import utilities.PropertyLoader;

public final class Login extends Page {
    // Usamos un Collator para obviar las diferencias por tildes o por mayúsculas en los nombres de las opciones.
    public Collator esCollator = Collator.getInstance(new Locale("es"));

    // Se declaran todos los objetos de la página
    @AndroidFindBy(id = "com.android.permissioncontroller:id/permission_allow_foreground_only_button")
    private WebElement permissionAllowBtn;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/full_login_nav_host")
    private WebElement fullLoginNavHost;
    // Welcome
    @AndroidFindBy(id = appPackage + ".fulllogin:id/spnCountrySelector")
    @iOSXCUITFindBy(accessibility = "Seleccionar país")
    private WebElement countrySelector;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/btn_login_in_welcome")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Inicia sesión' or @name='Fazer login']")
    private WebElement iniciarSesionBtn;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/btn_register_in_welcome")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Regístrate' or @name='Criar conta']")
    private WebElement registrateBtn;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/tv_invited_access_welcome")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Ingresa sin registrarte' or @name='Acessar sem cadastro']")
    private WebElement ingresaSinRegistrarteBtn;
    // Log In
    @AndroidFindBy(id = appPackage + ".fulllogin:id/tietEmailLogin")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=' Correo electrónico ' or @name=' Email ']")
    private WebElement logIn_email;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/tietPasswordLogin")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=' Contraseña ' or @name=' Senha ']")
    private WebElement logIn_password;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/btn_login_in_login")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Inicia sesión' or @name='Fazer login']")
    private WebElement logIn_logInBtn;
    @AndroidFindBy(id = appPackage + ":id/btnActionSecond")
    private WebElement declineTouchIdBtn;
    // Register Form 1
    @AndroidFindBy(id = appPackage + ".fulllogin:id/etFirstName")
    @HowToUseLocators(iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @iOSXCUITFindBy(accessibility = "  Nombres *  ")
    @iOSXCUITFindBy(accessibility = "  Nome *  ")
    private WebElement name;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/etLastName")
    @HowToUseLocators(iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @iOSXCUITFindBy(accessibility = "  Apellidos *  ")
    @iOSXCUITFindBy(accessibility = "  Sobrenome *  ")
    private WebElement lastName;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/etEmailAddress")
    @HowToUseLocators(iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @iOSXCUITFindBy(accessibility = "  Correo electrónico *  ")
    @iOSXCUITFindBy(accessibility = "  Email *  ")
    private WebElement email;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/spn_document_type")
    @iOSXCUITFindBy(accessibility = "Go Down")
    private WebElement documentTypeSelector;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/txtDocumentType")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeStaticText[9]")
    private WebElement documentTypeText;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/etDocument")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTextField[4]")
    private WebElement id;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/etTextPhoneNumber")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[starts-with(@value, '+')]")
    private WebElement phone;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/rlGender")
    private WebElement genderSelector;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/btn_continue_in_register")
    @iOSXCUITFindBy(accessibility = "Continuar")
    private WebElement continuarRegisterBtn;
    @AndroidFindBy(id = appPackage + ":id/textinput_error")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Formato inválido']")
    private WebElement textinputError;
    // Register Form 2
    @AndroidFindBy(id = appPackage + ".fulllogin:id/etPassword")
    @HowToUseLocators(iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @iOSXCUITFindBy(accessibility = "  Contraseña  ")
    @iOSXCUITFindBy(accessibility = "  Sehna  ")
    private WebElement password;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/etRepeatPassword")
    @HowToUseLocators(iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @iOSXCUITFindBy(accessibility = "  Repite la contraseña  ")
    @iOSXCUITFindBy(accessibility = "  Digite a senha novamente  ")
    private WebElement repeatPassword;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/checkboxTermsConditions")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeImage[@name='checkboxFillUnchecked'])[1]")
    private WebElement termsCondition_chbx;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/tvTermAndConditions")
    @HowToUseLocators(iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @iOSXCUITFindBy(accessibility = "Aceptar términos y condiciones")
    @iOSXCUITFindBy(accessibility = "Aceitar Termos e condições")
    private WebElement termsCondition_btn;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/tvTermsAndConds")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeWebView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
    private WebElement termsCondition_txt;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/checkboxPrivacyPolicies")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeImage[@name='checkboxFillUnchecked']")
    private WebElement privacyPolicy_chbx;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/tvPrivacyPolicies")
    @iOSXCUITFindBy(accessibility = "Aceptar políticas de privacidad")
    private WebElement privacyPolicy_btn;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/tvTermsAndConds")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeWebView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
    private WebElement privacyPolicy_txt;
    @AndroidFindBy(id = appPackage + ".fulllogin:id/btnNext")
    @HowToUseLocators(iOSXCUITAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @iOSXCUITFindBy(accessibility = "Crear contraseña")
    @iOSXCUITFindBy(accessibility = "Criar senha")
    private WebElement crearContrasenaBtn;

    public Login(Connection connection) {
        super(connection, AppiumBy.id(appPackage + ".fulllogin:id/full_login_nav_host"));
        
        esCollator.setStrength(Collator.PRIMARY);
    }

    public void checkPermissionsRequest() {
        try {
            midWait.until(ExpectedConditions.elementToBeClickable(permissionAllowBtn));
        }
        catch(Exception e) {
            //TODO: Escribir el error si no deja crear la cuenta
            System.out.println("No solicita permisos");
        }
    }

    public void allowPermissions() throws Exception {
        if (connection.isAndroid()) {
            permissionAllowBtn.click();
            WaitToLoad();
        }
        else if (connection.isIOS()) {
            midWait.until(ExpectedConditions.invisibilityOfElementLocated(
                AppiumBy.xpath(
                    "//XCUIElementTypeAlert"//[@name='“Spid” Would Like to Send You Notifications']"
                )
            ));
        }
        else {
            throw new Exception("Unimplemented OS");
        }
    }

    public void setCountry(String desiredCountry) throws Exception {        
        countrySelector.click();
        Boolean found = false;
        if (connection.isAndroid()) {
            List<WebElement> list = driver.findElements(
                AppiumBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/*")
            );
            for (WebElement element: list) {
                WebElement countryWE = element.findElement(AppiumBy.xpath(".//android.widget.TableRow/android.widget.TextView"));
                if (esCollator.equals(Generics.getNormalizedText(countryWE), desiredCountry)) {
                    element.click();
                    found = true;
                    break;
                }
            }
        }
        else if (connection.isIOS()) {
            try {
                driver.findElement(AppiumBy.xpath("//XCUIElementTypeButton[@name='" + desiredCountry + "']")).click();
                found = true;
            }
            catch (NoSuchElementException e) {}
        }
        else {
            throw new Exception("Unimplemented OS");
        }
        if (!found) {
            throw new Exception("Country \"" + desiredCountry + "\" was not found");
        }

        midWait.until(ExpectedConditions.visibilityOf(iniciarSesionBtn));
    }

    public void logIn(User user) throws Exception {
        iniciarSesionBtn.click();
        Generics.writeTextField(connection, logIn_email, user.getEmail());
        Generics.writeTextField(connection, logIn_password, user.getPassword());
        midWait.until(ExpectedConditions.elementToBeClickable(logIn_logInBtn)).click();
    }

    public void declineTouchId() {
        declineTouchIdBtn.click();
    }

    public void logInAsGuest() {
        ingresaSinRegistrarteBtn.click();
    }

    public void openRegisterForm() {
        registrateBtn.click();
    }

    public ArrayList<String> getDocumentTypes() throws Exception {
        ArrayList<String> displayedDocumentTypes = new ArrayList<String>();
        
        if (connection.isAndroid()) {
            try {
                documentTypeSelector.click();
                Thread.sleep(1000);
                for (WebElement element: driver.findElements(AppiumBy.xpath("//android.widget.CheckedTextView"))) {
                    displayedDocumentTypes.add(Generics.getNormalizedText(element));
                }
                driver.navigate().back();
            }
            catch (NoSuchElementException e) {
                displayedDocumentTypes.add(Generics.getNormalizedText(documentTypeText));
            }
        }
        else if (connection.isIOS()) {
            try {
                documentTypeSelector.click();
                for (WebElement element: driver.findElements(
                    AppiumBy.xpath(
                        "//XCUIElementTypeButton[XCUIElementTypeStaticText and XCUIElementTypeImage]"
                    )
                )) {
                    displayedDocumentTypes.add(Generics.getNormalizedText(element));
                }
            }
            catch (NoSuchElementException e) {
                displayedDocumentTypes.add(Generics.getNormalizedText(documentTypeText));
            }
        }
        else {
            throw new Exception("Unimplemented OS");
        }
        return displayedDocumentTypes;
    }

    private void selectDocumentType(String desiredDocumentType) throws Exception {
        documentTypeSelector.click();
        List<WebElement> list;
        if (connection.isAndroid()) {
            list = driver.findElements(AppiumBy.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/*"));
        }
        else if (connection.isIOS()) {
            list = driver.findElements(AppiumBy.xpath("//XCUIElementTypeButton[XCUIElementTypeStaticText and XCUIElementTypeImage]"));
        }
        else {
            throw new Exception("Unimplemented OS");
        }
        // Por cada opción dentro de la aplicación reviso si coincide con alguna de las opciones correctas.
        for (WebElement element: list) {
            if (esCollator.equals(Generics.getNormalizedText(element), desiredDocumentType)) {
                element.click();
                return;
            }
        }
        // TODO: Escribir el error de que una opción en la aplicación no coincide con ninguna de la lista oficial o está repetida.
        throw new Exception("No existe la opción deseada (" + desiredDocumentType + ")");
    }

    public void fillRegisterFormExceptId(Customer customer) throws Exception {
        Generics.writeTextField(connection, name, customer.getName());
        Generics.writeTextField(connection, lastName, customer.getLastName());
        Generics.writeTextField(connection, email, PropertyLoader.getProperty("unregisteredEmail"));
        if (connection.isIOS()) {
            Generics.writeTextField(connection, phone, customer.getPhone().substring(3));
        }
        else {
            Generics.writeTextField(connection, phone, customer.getPhone());
        }
        
        try {
            genderSelector.click();
            Thread.sleep(1000);
            for (WebElement element: driver.findElements(AppiumBy.xpath("//*"))) {
                if (Generics.getNormalizedText(element).equals(customer.getGender())) {
                    element.click();
                    break;
                }
            }
        }
        catch (NoSuchElementException e) {}
        catch (InterruptedException e) {}

        try {
            shortWait.until(ExpectedConditions.visibilityOf(textinputError));
            throw new Exception("Wrong data in first register form");
        }
        catch (TimeoutException e) {}
    }
    
    public void fillId(String documentType, String idNumber) throws Exception {
        try {
            selectDocumentType(documentType);
        }
        catch (NoSuchElementException e) {
            if (!Generics.getNormalizedText(documentTypeText).equals(documentType)) {
                // TODO: Escribir este error cuando el tipo de documento no existe
                throw new Exception();
            }
        }
        if (connection.isAndroid()) {
            try {
                Generics.androidDismissAutofill((AndroidDriver)driver);
            }
            catch (TimeoutException e) {}
        }
        if (connection.isIOS()) {
            id.clear();
        }
        Generics.writeTextField(connection, id, idNumber);
    }

    public Boolean checkForTextInputError() {
        try {
            midWait.until(ExpectedConditions.visibilityOfElementLocated(AppiumBy.id(appPackage + ":id/textinput_error")));
            return true;
        }
        catch (TimeoutException e) {
            return false;
        }
    }
        
    public void fillRegisterForm(Customer customer) throws Exception {
        fillRegisterFormExceptId(customer);
        fillId(customer.getDocumentType(), customer.getId());

        midWait.until(ExpectedConditions.elementToBeClickable(continuarRegisterBtn)).click();
    }

    public Boolean checkRightSized(String documentType, String idNumber) throws Exception {
        fillId(documentType, idNumber);
        midWait.until(ExpectedConditions.or(
            ExpectedConditions.elementToBeClickable(continuarRegisterBtn),
            ExpectedConditions.visibilityOf(textinputError)
        ));
        // Si al ingresar el id la app te lo cambió (Ej: Ingresaste un valor de 9 dígitos y sólo se
        // escribieron los primos 7), entonces podríamos decir que la app rechazó el input de 9 dígitos.
        if (continuarRegisterBtn.getAttribute("enabled").equals("true") && Generics.getNormalizedText(id).replaceAll("[^\\d]", "").equals(idNumber)) {
            return true;
        }
        return false;
    }

    public boolean checkTermsConditions() throws Exception {
        shortWait.until(
            ExpectedConditions.visibilityOf(termsCondition_btn)
        ).click();
        // Aquí tendría que esperar a que cargue el recurso
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {}
        if (connection.isAndroid()) {
            return (Generics.getNormalizedText(termsCondition_txt).replaceAll("\\W+", "").length() > 0);
        }
        else if (connection.isIOS()) {
            return (privacyPolicy_txt.findElements(
                AppiumBy.xpath(
                    ".//XCUIElementTypeOther/XCUIElementTypeStaticText"
                )
            ).size() > 1);
        }
        else {
            throw new Exception("Unimplemented OS");
        }
    }

    public boolean checkPrivacyPolicy() throws Exception {
        shortWait.until(
            ExpectedConditions.visibilityOf(privacyPolicy_btn)
        ).click();
        // Aquí tendría que esperar a que cargue el recurso
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {}
        if (connection.isAndroid()) {
            return (Generics.getNormalizedText(privacyPolicy_txt).replaceAll("\\W+", "").length() > 0);
        }
        else if (connection.isIOS()) {
            return (privacyPolicy_txt.findElements(
                AppiumBy.xpath(
                    ".//XCUIElementTypeOther/XCUIElementTypeStaticText"
                )
            ).size() > 6);
        }
        else {
            throw new Exception("Unimplemented OS");
        }
    }

    public void createPassword(Customer customer) throws Exception {
        Generics.writeTextField(connection, password, customer.getPassword());
        Generics.writeTextField(connection, repeatPassword, customer.getPassword());
        if (connection.isIOS()) {
            termsCondition_chbx.click();
            try {
                privacyPolicy_chbx.click();
            }
            catch (NoSuchElementException e) {}
        }
        else {
            if (termsCondition_chbx.getAttribute("checked").equals("false")) {
                termsCondition_chbx.click();
            }
            try {
                if (privacyPolicy_chbx.getAttribute("checked").equals("false")) {
                    privacyPolicy_chbx.click();
                }
            }
            catch (NoSuchElementException e) {}
        }
    }

    public void submitRegisterForm() throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(2));
        try {
            wait.until(ExpectedConditions.elementToBeClickable(crearContrasenaBtn));
        }
        catch(Exception e) {
            //TODO: Escribir el error si no deja crear la cuenta
            throw new Exception("Unable to register");
        }
    }
}
