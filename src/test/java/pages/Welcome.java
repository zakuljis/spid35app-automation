package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

import org.openqa.selenium.WebElement;

import utilities.Page;

public final class Welcome extends Page {
    //Se declaran todos los objetos de la página
    @AndroidFindBy(id = appPackage + ".fulllogin:id/btnContinue")
    @iOSXCUITFindBy(accessibility = "Ingresar / Entrar")
    private WebElement btnIngresar;

    public Welcome(AppiumDriver driver) {
        super(driver);
    }

    public void enter() {
        btnIngresar.click();
    }
}
