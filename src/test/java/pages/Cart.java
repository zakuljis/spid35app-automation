package pages;

import java.text.Collator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utilities.Connection;
import utilities.Generics;
import utilities.Page;

public class Cart extends Page {
    // Usamos un Collator para obviar las diferencias por tildes o por mayúsculas en los nombres de las opciones.
    public Collator esCollator = Collator.getInstance(new Locale("es"));
    
    @AndroidFindBy(id = appPackage + ".cart:id/btnPay")
    private WebElement goToCheckoutBtn;
    @AndroidFindBy(xpath = "//*[@content-desc='Delivery Edit icon']")
    private WebElement addressEditBtn;
    @AndroidFindBy(xpath = "//*[@resource-id='payment-selector-container']")
    private WebElement paymentMethods;
    @AndroidFindBy(xpath = "//*[@resource-id='payment-data']")
    private WebElement paymentData;

    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().resourceId(\"creditCardpayment-card-0Number\"))")
    private WebElement creditCardNumber;
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().resourceId(\"creditCardpayment-card-0Name\"))")
    private WebElement creditCardName;
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().resourceId(\"creditCardpayment-card-0Month\"))")
    private WebElement creditCardMonth;
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().resourceId(\"creditCardpayment-card-0Year\"))")
    private WebElement creditCardYear;
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().resourceId(\"creditCardpayment-card-0Code\"))")
    private WebElement creditCardCvv;
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().resourceId(\"holder-document-0\"))")
    private WebElement creditCardId;
    
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().resourceId(\"i-coupon\"))")
    private WebElement coupon;
    /*
    //@AndroidFindBy(accessibility = "Modificar carrito")
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().description(\"Modificar carrito\"))")
    private WebElement cartEditBtn;
    */
    @AndroidFindBy(uiAutomator = "new UiScrollable(new UiSelector().scrollable(true))" +
        ".scrollIntoView(new UiSelector().resourceId(\"payment-data-submit\"))")
    private WebElement submitBtn;

    public Cart(Connection connection) {
        super(connection, AppiumBy.id(appPackage + ".cart:id/cart_nav_host"));
        WaitToLoad();

        esCollator.setStrength(Collator.PRIMARY);
    }

    public HashMap<String, String[]> getCartPrices() {
        HashMap<String, String[]> prices = new HashMap<String, String[]>();
        ArrayList<String> normalPrices = new ArrayList<String>(), currentPrices = new ArrayList<String>(), amount = new ArrayList<String>();
        String normalPrice;
        for (WebElement product: driver.findElements(AppiumBy.xpath(
            String.format(".//*[@resource-id='%s.cart:id/rvMyCart']/*[@resource-id='%s.cart:id/clItemContainer']", appPackage, appPackage)
        ))) {
            amount.add(Generics.getNormalizedText(product.findElement(
                AppiumBy.xpath(".//*[@resource-id='" + appPackage + ".cart:id/txvProductAmount']")
            )));
            normalPrice = Generics.getNormalizedText(product.findElement(
                AppiumBy.xpath(".//*[@resource-id='" + appPackage + ".cart:id/txvProductCost']")
            ));
            normalPrices.add(normalPrice);
            // Veo si el producto está en descueto
            try {
                currentPrices.add(Generics.getNormalizedText(product.findElement(
                    AppiumBy.xpath(".//*[@resource-id='" + appPackage + ".cart:id/txvProductCostWithDiscount']")
                )));
            }
            // Si no hay un descuento, entonces el precio de oferta es el precio normal
            catch (NoSuchElementException e) {
                currentPrices.add(normalPrice);
            }
        }

        prices.put("normal", new String[normalPrices.size()]);
        prices.put("current", new String[currentPrices.size()]);
        prices.put("amount", new String[amount.size()]);
        normalPrices.toArray(prices.get("normal"));
        currentPrices.toArray(prices.get("current"));
        amount.toArray(prices.get("amount"));

        return prices;
    }

    public HashMap<String, String> getSubtotal() {
        HashMap<String, String> subtotal = new HashMap<String, String>();
        subtotal.put("current", Generics.getNormalizedText(goToCheckoutBtn));
        try {
            subtotal.put("normal", Generics.getNormalizedText(driver.findElement(
                AppiumBy.xpath(".//*[@resource-id='" + appPackage + ".cart:id/tvSubTotalPrice']")
            )));
            subtotal.put("discount", Generics.getNormalizedText(driver.findElement(
                AppiumBy.xpath(".//*[@resource-id='" + appPackage + ".cart:id/tvDiscountPrice']")
            )));
        }
        catch (NoSuchElementException e) {
            subtotal.put("normal", null);
            subtotal.put("discount", null);
        }
        return subtotal;
    }

    public void goToCheckout() {
        goToCheckoutBtn.click();
        WaitToLoad();
        longWait.until(
            ExpectedConditions.or(
                ExpectedConditions.visibilityOf(paymentData),
                ExpectedConditions.visibilityOf(paymentMethods)
            )
        );

        // Este Thread.sleep() es para parchar un bug que hacía que el
        // formulario de tarjeta de crédito no apareciera. Se recomienda
        // eliminar en el futuro.
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public HashSet<String> getPaymentMethods() {
        HashSet<String> methods = new HashSet<String>();
        for (WebElement element: paymentMethods.findElements(AppiumBy.xpath(".//*/android.view.View/*"))) {
            methods.add(Generics.getNormalizedText(element));
        }
        return methods;
    }

    public void selectPaymentMethod(String method) {
        for (WebElement element: paymentMethods.findElements(AppiumBy.xpath(".//*/android.view.View/*"))) {
            if (esCollator.equals(Generics.getNormalizedText(element), method)) {
                element.click();
                break;
            }
        }
    }

    public void fillPaymentForm(String country, String cardNumber, String fullName, String expirationMonth, String expirationYear, String cvv, String id) throws Exception {        
        longWait.until(
            ExpectedConditions.visibilityOf(paymentData)
        );
        
        Generics.writeTextField(connection, creditCardNumber, cardNumber);
        Generics.writeTextField(connection, creditCardName, fullName);
        creditCardMonth.click();
        Generics.checkViewSelect(driver, expirationMonth);
        creditCardYear.click();
        Generics.checkViewSelect(driver, expirationYear);
        Generics.writeTextField(connection, creditCardCvv, cvv);
        
        if (esCollator.equals("Colombia", country)) {
            try {
                Generics.writeTextField(connection, creditCardId, id);
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public void submitForm() {
        submitBtn.click();
    }
}
