package models;

public class Customer extends User {
    String name = "";
    String lastName = "";
    String email = "";
    String documentType = "";
    String id = "";
    String phone = "";
    String gender;

    public Customer(String email, String wrongEmail, String password, String wrongPassword, String address, String name, String lastName, String documentType, String id, String phone, String gender) {
        super(email, password, wrongEmail, wrongPassword, address);
        this.name = name;
        this.lastName = lastName;
        this.documentType = documentType;
        this.id = id;
        this.phone = phone;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.name = lastName;
    }
    public String getDocumentType() {
        return documentType;
    }
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
}
