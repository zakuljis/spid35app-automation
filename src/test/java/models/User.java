package models;

public class User {
    String email = "";
    String password = "";
    String wrongEmail = "";
    String wrongPassword = "";
    String address = "";

    public User(String email, String password, String wrongEmail, String wrongPassword, String address) {
        this.email = email;
        this.password = password;
        this.wrongEmail = wrongEmail;
        this.wrongPassword = wrongPassword;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getWrongEmail() {
        return wrongEmail;
    }
    public void setWrongEmail(String wrongEmail) {
        this.wrongEmail = wrongEmail;
    }
    public String getWrongPassword() {
        return wrongPassword;
    }
    public void setWrongPassword(String wrongPassword) {
        this.wrongPassword = wrongPassword;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}
