package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

public class DEPRECATEDPropertyLoader {
    public String loadProperty(String parameter)
    {
        return System.getProperty(parameter);
    }

    public Properties loadProperties() {
        Properties props = new Properties();
        String filePath = new File("params.properties").getAbsolutePath();
        File file = new File(filePath);
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
            props.load(new InputStreamReader(fileInput, Charset.forName("UTF-8")));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (fileInput != null) {
                try {
                    fileInput.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return props;
    }
}
