package utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileNotFoundException;
import java.net.URL;
import java.time.Duration;

public class Connection {
    private AppiumDriver driver;

    public AppiumDriver getDriver() {
        return driver;
    }

    public void init() throws Exception {
        // Carga del fichero de propiedades
        //DEPRECATEDPropertyLoader loadproperty = new DEPRECATEDPropertyLoader();
        // Recuperación del fichero de propiedades de la ruta y nombre de la aplicación móvil
        String appiumON = PropertyLoader.getProperty("appiumON");
        String isAndroid = PropertyLoader.getProperty("isAndroid");

        String hostMaquina = PropertyLoader.getProperty("hostMaquina");
        String appName = PropertyLoader.getProperty("appName");

        String deviceNameIOS = PropertyLoader.getProperty("deviceNameIOS");
        String platformNameIOS = PropertyLoader.getProperty("platformNameIOS");
        String platformVersionIOS = PropertyLoader.getProperty("platformVersionIOS");
        String bundleIdIOS = PropertyLoader.getProperty("bundleIdIOS");
        String automationNameIOS = PropertyLoader.getProperty("automationNameIOS");
        String appPathIOS = PropertyLoader.getProperty("appPathIOS");

        String platformNameAND = PropertyLoader.getProperty("platformNameAND");
        String deviceNameAND = PropertyLoader.getProperty("deviceNameAND");
        String avd = PropertyLoader.getProperty("avd");
        String appPackage = PropertyLoader.getProperty("appPackage");
        String appActivity = PropertyLoader.getProperty("appActivity");
        String appWaitActivity = PropertyLoader.getProperty("appWaitActivity");
        String automationNameAND = PropertyLoader.getProperty("automationNameAND");
        String noReset = PropertyLoader.getProperty("noReset");
        // Generación de las capabilites a nivel del servicio de Appium
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
        // Generación de las capabilites a nivel de driver
        DesiredCapabilities clientCapabilities = new DesiredCapabilities();
        clientCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        clientCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
        
        URL url;
        DesiredCapabilities cap;
        try {
            if (appiumON.equals("S")) {
                System.out.println("Appium web service");
                url = new URL("http://" + PropertyLoader.getProperty("AppiumServerIP") + ":" + PropertyLoader.getProperty("AppiumServerPort") + "/wd/hub");
                cap = clientCapabilities;
            } 
            else {
                cap = new DesiredCapabilities();
                url = new URL("http://" + hostMaquina + ":4723/wd/hub");
                if (isAndroid.contains("S")) {
                    System.out.println("Appium local");
                    System.out.println("Nombre dispositivo: " + deviceNameAND);
                    System.out.println("Nombre plataforma: " + platformNameAND);
                    System.out.println("Nombre automatizacion: " + automationNameAND);
                    System.out.println("Nombre aplicacion: " + appName);
                    
                    cap.setCapability("platformName", platformNameAND);
                    cap.setCapability("deviceName", deviceNameAND);
                    cap.setCapability("avd", avd);
                    cap.setCapability("appPackage", appPackage);
                    cap.setCapability("appActivity", appActivity);
                    cap.setCapability("appWaitActivity", appWaitActivity);
                    cap.setCapability("automationName", automationNameAND);
                    cap.setCapability("noReset", noReset);
                    
                    driver = new AndroidDriver(url, cap);
                }
                else {
                    System.out.println("Appium local");
                    System.out.println("Nombre dispositivo: " + deviceNameIOS);
                    System.out.println("Nombre plataforma: " + platformNameIOS);
                    System.out.println("Nombre automatizacion: " + automationNameIOS);
                    System.out.println("Nombre aplicacion: " + appName);
                    
                    cap.setCapability("deviceName", deviceNameIOS);
                    cap.setCapability("platformName", platformNameIOS);
                    cap.setCapability("platformVersion", platformVersionIOS);
                    cap.setCapability("appName", appName);
                    cap.setCapability("automationName", automationNameIOS);
                    cap.setCapability("app", appPathIOS);
                    
                    cap.setCapability("autoAcceptAlerts", true);
                    cap.setCapability("permissions", "{\"" + bundleIdIOS + "\":{\"location\":\"inuse\"}}");
                    cap.setCapability("simpleIsVisibleCheck", true);
                    cap.setCapability("useJSONSource", true);
                    
                    driver = new IOSDriver(url, cap);
                }
            }
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        } catch (Exception e) {
            throw new Exception ("Error connecting to Appium service : " + e.getMessage());
        }
    }

    public boolean isAndroid() throws FileNotFoundException {
        String isAndroid = PropertyLoader.getProperty("isAndroid");
        if (isAndroid.contains("S")) {
            return true;
        }
        return false;
    }

    public boolean isIOS() throws FileNotFoundException {
        String isAndroid = PropertyLoader.getProperty("isAndroid");
        if (isAndroid.contains("S")) {
            return false;
        }
        return true;
    }

    public void tearDown() {
        if(driver != null) {
            driver.quit();
        }
    }
}
