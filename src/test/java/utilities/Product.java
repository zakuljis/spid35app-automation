package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

public class Product {
    static final private int MAXCOUNT = 6;

    private SearchContext container;
    private By firstAdd;
    private By add;
    private By quantity;
    private By firstRemove;
    private By remove;
    private int count = 0;

    public Product(SearchContext container, By firstAddBtn, By addBtn, By quantityText, By firstRemoveBtn, By removeBtn) {
        this.container = container;
        firstAdd = firstAddBtn;
        add = addBtn;
        quantity = quantityText;
        firstRemove = firstRemoveBtn;
        remove = removeBtn;

        try {
            count = Integer.parseInt(Generics.getNormalizedText(container.findElement(quantity)));
        }
        catch (NoSuchElementException e) {}
    }

    public int getCount() {
        return count;
    }

    private WebElement getButton(By button) {
        WebElement desired;
        try {
            desired = container.findElement(button);
        }
        catch (NoSuchElementException e) {
            container.findElement(quantity).click();
            desired = container.findElement(button);
        }
        return desired;
    }

    public void Add(int number) throws Exception {
        if (number < 1) {
            // TODO: Escribir la excepción de cantidad de clicks inválida.
            throw new Exception();
        }
        else if (count + number > MAXCOUNT) {
            // TODO: Escribir esta excepción
            throw new Exception();
        }
        
        int i = 0;
        if (count < 1) {
            getButton(firstAdd).click();
            count++;
            i++;
        }
        
        for (; i < number; i++) {
            getButton(add).click();
            count++;
        }
    }

    public void Add() throws Exception {
        Add(1);
    }

    public void Remove(int number) throws Exception {
        if (number < 1) {
            // TODO: Escribir la excepción de cantidad de clicks inválida.
            throw new Exception();
        }
        else if (number > count) {
            // TODO: Escribir la excepción de cantidad de clicks inválida.
            throw new Exception();
        }

        for (int i = 0; i < number; i++) {
            if (count == 1) {
                getButton(firstRemove).click();
            }
            else {
                getButton(remove).click();
            }
            count--;
        }
    }

    public void Remove() throws Exception {
        Remove(1);
    }

    public void RemoveAll() throws Exception {
        Remove(count);
    }
}
