package utilities;

import io.appium.java_client.AppiumBy;
//import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HasOnScreenKeyboard;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.TouchAction;
//import java.util.concurrent.TimeUnit;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

import java.io.FileNotFoundException;
import java.text.Normalizer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Generics extends Connection {

    AppiumDriver driver;

    public static boolean waitPresent(AppiumDriver driver, WebElement element) {
        try {
            element.isDisplayed();
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public static void androidDismissAutofill(AndroidDriver driver) {
        new WebDriverWait(driver, Duration.ofSeconds(2)).until(
            ExpectedConditions.presenceOfElementLocated(
                AppiumBy.xpath("/hierarchy/android.widget.FrameLayout[@resource-id='android:id/autofill_dataset_picker']")
            )
        );
        driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
    }

    public static void androidCheckViewSelect(AndroidDriver driver, String option) {
        driver.findElement(AppiumBy.androidUIAutomator(
            "new UiScrollable(new UiSelector().scrollable(true))" +
            ".scrollIntoView(new UiSelector().resourceId(\"android:id/text1\").text(\"" + option + "\"))"
        )).click();
    }

    public static void checkViewSelect(AppiumDriver driver, String option) {
        // if AND:
        androidCheckViewSelect((AndroidDriver)driver, option);
    }

    public static void hideKeyboard(Connection connection) throws Exception {
        if (connection.isAndroid()) {
            while (((AndroidDriver)connection.getDriver()).isKeyboardShown()) {
                try {
                    ((AndroidDriver)connection.getDriver()).hideKeyboard();
                }
                catch (WebDriverException e) {
                    System.out.println(e.getMessage().split("\\n")[0] + "\nRetrying in 1 second...");
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }
        else if (connection.isIOS()) {
            // Probé todas las formas que se me ocurrieron para cerrar el teclado:
            // driver.hideKeyboard(), buscar el boton "Done" o "Return" por findId o findName,
            // usar hideKeyboardStrategy, pero nada funcionó, así que tuve que hacer esto.
            HashMap<String, Object> args = new HashMap<>();
            args.put("x", 205);
            args.put("y", 80);
            connection.getDriver().executeScript("mobile: tap", args);
        }
    }

    public static void writeTextField(Connection connection, WebElement element, String text) throws Exception {
        if (connection.isAndroid()) {
            element.click();
            try {
                androidDismissAutofill((AndroidDriver)connection.getDriver());
            }
            catch (TimeoutException e) {}
        }
        element.sendKeys(text);
        if (connection.isAndroid() || connection.isIOS()) {
            hideKeyboard(connection);
        }
    }

    public static String getNormalizedText(WebElement element) {
        return Normalizer.normalize(element.getText().strip(), Normalizer.Form.NFC);
    }

    public static ArrayList<Integer> getRange(String range) throws Exception {
        // TODO: Hacer mejor esta excepción, indicando el tipo InvalidRange o Format o algo así
        Exception e = new Exception("\"" + range + "\" is not a Range");
        
        Matcher matcher = Pattern.compile("(?<start>(?:\\+|-)?[0-9]+)(?:\\.\\.(?<end>(?:\\+|-)?[0-9]+))?").matcher(range);
        if (!matcher.matches()) {
            throw e;
        }

        Integer start = Integer.valueOf(matcher.group("start"));
        ArrayList<Integer> rangeList = new ArrayList<Integer>();
        rangeList.add(start);
        if (matcher.group("end") != null) {
            Integer end = Integer.valueOf(matcher.group("end"));
            if (end - start < 0) {
                throw e;
            }

            while (start < end) {
                rangeList.add(++start);
            }
        }
        return rangeList;
    }

    /*
    public static void scrollAndroid(AppiumDriver driver) {
        try {
            driver.findElement(AppiumBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollToEnd(10)"));
        }
        catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    public static void scrollToBeginningAndroid(AppiumDriver driver) {
        try {
            driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollToBeginning(10)"));
        }
        catch (Exception e){
            System.err.println(e.getMessage());
        }
    }
    */
}
