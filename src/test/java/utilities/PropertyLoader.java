package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class PropertyLoader {
    private static Map<String, Object> data = null;
    
    public static <T extends Object> T getProperty(String parameter) throws FileNotFoundException
    {
        if (data == null) {
            loadProperties();
        }
        return (T)data.get(parameter);
    }

    private static void loadProperties() throws FileNotFoundException {
        String filePath = new File("params.yml").getAbsolutePath();
        FileInputStream fileInput = new FileInputStream(new File(filePath));
        Yaml yaml = new Yaml();
        data = (Map<String, Object>)yaml.load(fileInput);
    }
}
