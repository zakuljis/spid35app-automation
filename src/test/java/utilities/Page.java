package utilities;

import java.time.Duration;

import org.apache.commons.lang3.ObjectUtils.Null;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public abstract class Page {
    protected Connection connection = null;
    protected AppiumDriver driver;
    protected static final String appPackage = "com.cencosud.reg.spid35app" + ".dev";
    protected WebDriverWait shortWait, midWait, longWait;
    protected By[] root;

    public Page(Connection connection) {
        this(connection.getDriver());
        this.connection = connection;
    }

    public Page(Connection connection, By... root) {
        this(connection.getDriver(), root);
        this.connection = connection;
    }
    
    //FIXME: DEPRECATED
    public Page(AppiumDriver driver) {
        this(driver, new By[0]);
    }
    
    //FIXME: DEPRECATED
    public Page(AppiumDriver driver, By... root) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

        // 2 segundos es lo mismo que el implicit wait. Cambiar esto a 5[s] y midWait a 10[s]
        shortWait = new WebDriverWait(driver, Duration.ofSeconds(2));
        midWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        longWait = new WebDriverWait(driver, Duration.ofSeconds(20));

        this.root = root;
    }

    protected void WaitToLoad() {
        if (root.length == 0) {
            return;
        }

        ExpectedCondition<WebElement>[] conditions = new ExpectedCondition[root.length];
        for (int i = 0; i < root.length; i++) {
            conditions[i] = ExpectedConditions.presenceOfElementLocated(root[i]);
        }

        longWait.until(
            ExpectedConditions.or(conditions)
        );
    } 
}
