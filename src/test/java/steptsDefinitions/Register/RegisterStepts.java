package steptsDefinitions.Register;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.google.common.base.Strings;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

import models.Customer;
import pages.*;
import utilities.Connection;
import utilities.Generics;
import utilities.PropertyLoader;

@RunWith(Cucumber.class)
public class RegisterStepts extends Connection {
    Connection connection;
    Customer customer;
    Login loginPage;

    @Before
    public void makeConnection() throws Exception {
        connection = new Connection();
        connection.init();
    }

    @After
    public void closeConnection() {
        connection.tearDown();
    }
    
    @Given("^we are customer$")
    public void we_are_customer() throws Throwable {
        // ¿Podrían existir versiones Wrong de estos parámetros?
        customer = new Customer(
            PropertyLoader.getProperty("email"),
            PropertyLoader.getProperty("wrongEmail"),
            PropertyLoader.getProperty("password"),
            PropertyLoader.getProperty("wrongPassword"),
            null,//PropertyLoader.getProperty("address"),
            PropertyLoader.getProperty("name"),
            PropertyLoader.getProperty("lastName"),
            null,//PropertyLoader.getProperty("documentType"),
            null,//PropertyLoader.getProperty("id"),
            null,
            PropertyLoader.getProperty("gender")
        );
    }

    @And("^we enter to SpidApp$")
    public void we_enter_to_SpidApp() {
        Welcome welcome = new Welcome(connection.getDriver());
        welcome.enter();
    }

    @And("we allow the permissions")
    public void we_allow_the_permissions() throws Exception {
        loginPage = new Login(connection);
        loginPage.allowPermissions();
    }

    @Given("we are in {word}")
    public void we_are_from(String country) throws Exception {
        loginPage.setCountry(country);
        HashMap<String, String> propByCountry;
        propByCountry = PropertyLoader.getProperty("phone");
        customer.setPhone(propByCountry.get(country));
        
        propByCountry = PropertyLoader.getProperty("address");
        customer.setAddress(propByCountry.get(country));
        
        propByCountry = PropertyLoader.getProperty("documentType");
        customer.setDocumentType(propByCountry.get(country));
        
        propByCountry = PropertyLoader.getProperty("id");
        customer.setId(propByCountry.get(country));
    }

    @When("we open the register form")
    public void we_enter_the_register_page() {
        loginPage.openRegisterForm();
    }

    @Then("the list of document types shown belongs to {word}")
    public void the_list_of_identity_documents_shown_belongs_to(String country) throws Exception {
        HashMap<String, HashMap<String, String>> documentTypesByCountry = PropertyLoader.getProperty("documentTypes");
        HashSet<String> documentTypes = new HashSet<String>(documentTypesByCountry.get(country).keySet());
        
        ArrayList<String> displayedDocumentTypesArray = loginPage.getDocumentTypes();
        HashSet<String> displayedDocumentTypes = new HashSet<String>(displayedDocumentTypesArray);
        // TODO: Escribir la excepción "Elementos repetidos"
        assertTrue(displayedDocumentTypes.size() == displayedDocumentTypesArray.size());
        // TODO: Escribir la excepción "La cantidad de opciones mostradas por la app no coinciden con las establecidas para el país"
        assertTrue(displayedDocumentTypes.size() == documentTypes.size());
        Boolean exist;
        for (String displayedDocumentType: displayedDocumentTypes) {
            exist = false;
            for (String documentType: documentTypes) {
                if (loginPage.esCollator.equals(displayedDocumentType, documentType)) {
                    exist = true;
                    break;
                }
            }
            // TODO: Escribir la excepción "La opción mostrada no existe/es incorrecta"
            assertTrue(displayedDocumentType, exist);
        }
    }

    @And("we fill it all with valid data except the identity document")
    public void we_fill_it_all_with_valid_data_except_the_identity_document() throws Exception {
        loginPage.fillRegisterFormExceptId(customer);
        // TODO: Escribir este error
        assertFalse(loginPage.checkForTextInputError());
    }

    @Then("only the right-sized id numbers for each of the {word}'s document types let us to continue")
    public void only_the_right_sized_id_numbers_let_us_to_continue(String country) throws Exception {
        HashMap<String, HashMap<String, String>> documentTypes = PropertyLoader.getProperty("documentTypes");

        for (Map.Entry<String, String> entry: documentTypes.get(country).entrySet()) {
            ArrayList<Integer> range = Generics.getRange(entry.getValue());
            
            Integer[] wrongSizes;
            if (range.get(0) - 1 > 0) {
                wrongSizes = new Integer[] {range.get(0) - 1, range.get(range.size() - 1) + 1};
            }
            else {
                wrongSizes = new Integer[] {range.get(range.size() - 1) + 1};
            }

            for (Integer wrongSize: wrongSizes) {
                assertFalse(
                    String.format("Wrong-sized (%d) %s %s number (Size range: %s) was accepted", wrongSize, country, entry.getKey(), entry.getValue()),
                    loginPage.checkRightSized(entry.getKey(), Strings.repeat(String.format("%d", Math.abs(wrongSize)%10), wrongSize))
                );
            }
            for (Integer size: range) {
                assertTrue(
                    String.format("Right-sized (%d) %s %s number (Size range: %s) was not accepted", size, country, entry.getKey(), entry.getValue()),
                    loginPage.checkRightSized(entry.getKey(), Strings.repeat(String.format("%d", Math.abs(size)%10), size))
                );
            }
        }
    }

    @And("^we make register with right values$")
    public void we_make_register_with_right_values() throws Exception {
        loginPage.fillRegisterForm(customer);
    }

    @Then("^we can see the terms and conditions$")
    public void we_can_see_the_terms_and_conditions() throws Exception {
        assertTrue("Terms and Conditions are not visible", loginPage.checkTermsConditions());
    }

    @Then("we can see SpidApp's privacy policy")
    public void we_can_see_spidapp_s_privacy_policy() throws Exception {
        assertTrue("The Privacy Policy are not visible", loginPage.checkPrivacyPolicy());
    }

    @And("^we create valid password$")
    public void we_create_valid_password() throws Exception {
        loginPage.createPassword(customer);
    }

    @Then("^the register is successfull$")
    public void the_register_is_successfull() throws Exception {
        loginPage.submitRegisterForm();
        // TODO: Esto es más o menos incomprobable y aún hace falta el
        // cómo verficicar que el formulario se envió correctamente.
    }
}
