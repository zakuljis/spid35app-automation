package steptsDefinitions.Availability;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;

import java.text.Collator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.*;

import models.Customer;
import pages.*;
import pages.Home.SellerError;
import utilities.Connection;
import utilities.Generics;
import utilities.PropertyLoader;

@RunWith(Cucumber.class)
public class AvailabilityStepts {
    private HashMap<String, String[]> validAddressByCountry = new HashMap<String, String[]>();
    private HashMap<String, String[]> invalidAddressByCountry = new HashMap<String, String[]>();
    HashMap<SellerError, String> sellerErrorMessage = new HashMap<SellerError, String>();
    // Usamos un Collator para obviar las diferencias por tildes o por mayúsculas en los nombres de las opciones.
    private Collator esCollator = Collator.getInstance(new Locale("es"));
    
    Connection connection;
    Customer customer;
    Login loginPage;
    Home homePage;

    @Before
    public void makeConnection() throws Exception {
        connection = new Connection();
        connection.init();
    }

    @After
    public void closeConnection() {
        connection.tearDown();
    }

    @Given("^we are customer$")
    public void we_are_customer() throws Throwable {
        esCollator.setStrength(Collator.PRIMARY);
        // ¿Podrían existir versiones Wrong de estos parámetros?
        customer = new Customer(
            PropertyLoader.getProperty("email"),
            PropertyLoader.getProperty("wrongEmail"),
            PropertyLoader.getProperty("password"),
            PropertyLoader.getProperty("wrongPassword"),
            null,//PropertyLoader.getProperty("address"),
            PropertyLoader.getProperty("name"),
            PropertyLoader.getProperty("lastName"),
            null,//PropertyLoader.getProperty("documentType"),
            null,//PropertyLoader.getProperty("id"),
            null,
            PropertyLoader.getProperty("gender")
        );

        validAddressByCountry.put("Colombia", new String[] {
            "Avenida Carrera 9 #125-30",
            "Calle 151 #13A-50",
            "Calle 142 #12b-51",
            "Calle 146 #13-70",
            "Calle 97a #9a-50",
            "Carrera 15 #55-21",
            "Calle 93 #92-81"
        });
        invalidAddressByCountry.put("Colombia", new String[] {
            "Los Cedros #238",
            "Av. El Dorado #33a21 6775"
        });
        
        validAddressByCountry.put("Perú", new String[] {});
        invalidAddressByCountry.put("Perú", new String[] {});

        sellerErrorMessage.put(SellerError.OUTOFHOURS, PropertyLoader.getProperty("sellerErrorOutOfHours"));
        sellerErrorMessage.put(SellerError.OUTOFCOVERAGE, PropertyLoader.getProperty("sellerErrorOutOfCoverage"));
    }

    @And("^we enter to SpidApp$")
    public void we_enter_to_SpidApp() throws Throwable {
        Welcome welcome = new Welcome(connection.getDriver());
        welcome.enter();
    }

    @Given("^we are in Colombia$")
    public void we_are_from_Colombia() throws Throwable {
        loginPage = new Login(connection);
        loginPage.setCountry("Colombia");
        HashMap<String, String> propByCountry; 
        
        propByCountry = PropertyLoader.getProperty("phone");
        customer.setPhone(propByCountry.get("Colombia"));
        
        propByCountry = PropertyLoader.getProperty("address");
        customer.setAddress(propByCountry.get("Colombia"));
        
        propByCountry = PropertyLoader.getProperty("documentType");
        customer.setDocumentType(propByCountry.get("Colombia"));
        
        propByCountry = PropertyLoader.getProperty("id");
        customer.setId(propByCountry.get("Colombia"));
    }

    @When("^we log in as a guest$")
    public void we_log_in_as_a_guest() throws Throwable {
        loginPage.logInAsGuest();
    }

    @And("^we allow the permissions$")
    public void we_allow_the_permissions() throws Throwable {
        homePage = Home.createInstance(connection);
        //TODO: Cambiar esto, ahora los permisos se solicitan en el Login
        //homePage.allowPermissions();
    }
    
    HashSet<String> trues = new HashSet<String>(){{
        add("valid");
        add("should");
        add("within");
    }};
    HashSet<String> falses = new HashSet<String>(){{
        add("invalid");
        add("shouldnt");
        add("out-of");
    }};

    @ParameterType("[^\\s]+")
    public Boolean bool(String bool) throws Exception {
        if (trues.contains(bool)) {
            return true;
        }
        else if (falses.contains(bool)) {
            return false;
        }
        throw new Exception("\"" + bool + "\" cannot be parsed as a Boolean");
    }

    String currentAddr;
    String foundAddr;
    @And("we search a {bool} address")
    public void we_search_a_address(Boolean status) throws Throwable {
        // TODO: Cambiar esto para que se prueben todas las direcciones
        currentAddr = status? validAddressByCountry.get("Colombia")[0]: invalidAddressByCountry.get("Colombia")[0];
        WebElement foundAddrWE = homePage.searchAddress(currentAddr);
        foundAddr = foundAddrWE == null? null: Generics.getNormalizedText(foundAddrWE);
    }

    @Then("the app {bool} find that address")
    public void the_app_find_that_address(Boolean result) throws Throwable {
        if (result) {
            assertTrue(esCollator.equals(foundAddr, currentAddr));
        }
        else {
            assertNull(foundAddr);
        }
    }

    @When("we set a valid address {bool} range")
    public void we_set_a_valid_address_range(Boolean coverage) throws Throwable {
        // TODO: Cambiar esto para que se prueben todas las direcciones
        String currentAddr = coverage? validAddressByCountry.get("Colombia")[0]: "Calle 17 Bis #11945";
        homePage.setAddress(currentAddr);
    }

    @Then("the app {bool} allow us to search products")
    public void the_app_allow_us_to_search_products(Boolean result) throws Throwable {
        if (result) {
            assertTrue(homePage.checkCoverage());
        }
        else {
            String sellerError = homePage.getSellerError();
            assertTrue("Seller Error Message mismatch\n\tExpected: " + sellerErrorMessage.get(SellerError.OUTOFCOVERAGE) + "\n\t   Found: " + sellerError, esCollator.equals(sellerError, sellerErrorMessage.get(SellerError.OUTOFCOVERAGE)));
        }
    }

    @When("we set a coveraged address out of hours")
    public void we_set_a_coveraged_address_out_of_hours() throws Exception {
        homePage.setAddress(validAddressByCountry.get("Colombia")[0]);
    }

    @Then("the app should tell us that the store is closed")
    public void the_app_should_tell_us_that_the_store_is_closed() throws Exception {
        String sellerError = homePage.getSellerError();
        assertTrue("Seller Error Message mismatch\n\tExpected: " + sellerErrorMessage.get(SellerError.OUTOFHOURS) + "\n\t   Found: " + sellerError, esCollator.equals(sellerError, sellerErrorMessage.get(SellerError.OUTOFHOURS)));
    }
}
