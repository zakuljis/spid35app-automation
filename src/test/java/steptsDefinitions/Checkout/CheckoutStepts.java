package steptsDefinitions.Checkout;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.runner.RunWith;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import models.Customer;
import pages.*;
import utilities.Connection;
import utilities.PropertyLoader;

@RunWith(Cucumber.class)
public final class CheckoutStepts {
    Customer customer;
    Connection connection;
    Login loginPage;
    Home homePage;
    Cart cartPage;
    Order orderPage;

    List<HashMap<String, String>> creditCards;
    Pattern currencyPattern;

    @Before
    public void makeConnection() throws Exception {
        connection = new Connection();
        connection.init();
    }

    @After
    public void closeConnection() {
        connection.tearDown();
    }

    @Given("we are customer")
    public void we_are_user() throws Throwable {
        customer = new Customer(
            PropertyLoader.getProperty("email"),
            PropertyLoader.getProperty("wrongEmail"),
            PropertyLoader.getProperty("password"),
            PropertyLoader.getProperty("wrongPassword"),
            null,
            PropertyLoader.getProperty("name"),
            PropertyLoader.getProperty("lastName"),
            null,
            null,
            null,
            PropertyLoader.getProperty("gender")
        );

        creditCards = PropertyLoader.getProperty("creditCards");
    }

    @And("we enter to SpidApp")
    public void we_enter_to_SpidApp() throws Throwable {
        Welcome welcome = new Welcome(connection.getDriver());
        welcome.enter();
    }

    @And("we allow the permissions")
    public void we_allow_the_permissions() throws Exception {
        loginPage = new Login(connection);
        loginPage.allowPermissions();
    }

    @Given("we are in {word}")
    public void we_are_from_Colombia(String country) throws Throwable {
        loginPage.setCountry(country);

        HashMap<String, String> propByCountry;
        propByCountry = PropertyLoader.getProperty("phone");
        customer.setPhone(propByCountry.get(country));
        
        propByCountry = PropertyLoader.getProperty("address");
        customer.setAddress(propByCountry.get(country));
        
        propByCountry = PropertyLoader.getProperty("documentType");
        customer.setDocumentType(propByCountry.get(country));
        
        propByCountry = PropertyLoader.getProperty("id");
        customer.setId(propByCountry.get(country));

        propByCountry = PropertyLoader.getProperty("currencyRegEx");
        currencyPattern = Pattern.compile(propByCountry.get(country));
    }

    @And("we log in")
    public void we_log_in() throws Exception {
        loginPage.logIn(customer);
        loginPage.declineTouchId();
    }

    @And("we use or set a valid address")
    public void we_use_or_set_a_valid_address() throws Exception {
        homePage = Home.createInstance(connection);
        homePage.useOrSetAddress(customer.getAddress());
    }
        
    @When("we go to cart detail")
    public void we_go_to_cart_detail() {
        // Esperar a que el carrito cargue
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {}
        homePage.goToCartDetail();
        cartPage = new Cart(connection);
    }

    @Then("the subtotal and discounts will be right")
    public void the_subtotal_and_discounts_will_be_right() {
        Matcher matcher;
        String price;
        Float value;

        HashMap<String, String[]> cartPrices = cartPage.getCartPrices();
        HashMap<String, Float> realSubtotal = new HashMap<String, Float>();
        
        for (String key: new String[] {"normal", "current"}) {
            for (int i = 0; i < cartPrices.get(key).length; i++) {
                price = cartPrices.get(key)[i];
                matcher = currencyPattern.matcher(price);
                assertTrue("Price doesn't follow the currency format", matcher.matches());
                value = Float.parseFloat(matcher.group("units").replaceAll("\\D", ""));
                value += Float.parseFloat("0." + matcher.group("cents").replaceAll("\\D", ""));
                realSubtotal.merge(key, value*Integer.parseInt(cartPrices.get("amount")[i]), Float::sum);
            }
        }

        HashMap<String, String> subtotal = cartPage.getSubtotal();
        
        matcher = currencyPattern.matcher(subtotal.get("current"));
        assertTrue("Final price doesn't follow the currency format", matcher.find());
        value = Float.parseFloat(matcher.group("units").replaceAll("\\D", ""));
        value += Float.parseFloat("0." + matcher.group("cents").replaceAll("\\D", ""));
        assertTrue("Final price is wrong (It's " + value.toString() + " instead of " + realSubtotal.get("current").toString() + ")", value.equals(realSubtotal.get("current")));
        
        // Si no hay descuentos, entonces la pestaña de descuentos no se despliega, por lo que no es necesario seguir comprobando
        if (realSubtotal.get("current").equals(realSubtotal.get("normal"))) {
            return;
        }

        matcher = currencyPattern.matcher(subtotal.get("normal"));
        assertTrue("The subtotal doesn't follow the currency format", matcher.matches());
        value = Float.parseFloat(matcher.group("units").replaceAll("\\D", ""));
        value += Float.parseFloat("0." + matcher.group("cents").replaceAll("\\D", ""));
        assertTrue("The subtotal is wrong  (It's " + value + " instead of " + realSubtotal.get("normal"), value.equals(realSubtotal.get("normal")));
        
        matcher = currencyPattern.matcher(subtotal.get("discount"));
        assertTrue("Discounts doesn't follow the currency format", matcher.matches());
        value = Float.parseFloat(matcher.group("units").replaceAll("\\D", ""));
        value += Float.parseFloat("0." + matcher.group("cents").replaceAll("\\D", ""));
        assertTrue("Discounts are wrong  (They're " + value + " instead of " + (realSubtotal.get("normal") - realSubtotal.get("current")), value.equals(realSubtotal.get("normal") - realSubtotal.get("current")));
    }

    @And("we add an item to the cart")
    public void we_add_an_item_to_the_cart() throws Exception  {
        homePage.addProductToCart();
    }

    @And("we go to the checkout")
    public void we_go_to_the_checkout() {
        homePage.goToCartDetail();
        cartPage = new Cart(connection);
        cartPage.goToCheckout();
    }

    @And("we select {string} payment method")
    public void we_select_a_payment_method(String paymentMethod) {
        cartPage.selectPaymentMethod(paymentMethod);
    }
    
    @When("we {word} fill the payment form with a valid credit card in {word}")
    public void we_fill_the_payment_form(String state, String country) throws Exception {
        if (state.equals("shouldnt")) {
            return;
        }
        
        Boolean flag = true;
        for (HashMap<String, String> card: creditCards) {
            if (card.get("status").equals("valid")) {
                cartPage.fillPaymentForm(
                    country,
                    card.get("number"),
                    card.get("name"),
                    card.get("month"),
                    card.get("year"),
                    card.get("cvv"),
                    customer.getId()
                );
                flag = false;
                break;
            }
        }
        if (flag) {
            // TODO: Escribir el error cuando no hay una tarjea de crédito con el mismo status que el solicitado
            throw new Exception();
        }
    }

    @And("we submit the payment form")
    public void we_submit_the_payment_form() {
        cartPage.submitForm();
    }

    @Then("the checkout will be successful")
    public void the_checkout_will_be_successful() {
        orderPage = new Order(connection.getDriver());
    }
}

