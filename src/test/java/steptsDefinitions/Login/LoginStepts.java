package steptsDefinitions.Login;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

import models.User;
import pages.*;
import utilities.Connection;
import utilities.DEPRECATEDPropertyLoader;

@RunWith(Cucumber.class)
public class LoginStepts extends Connection {
    DEPRECATEDPropertyLoader loadproperty = new DEPRECATEDPropertyLoader();
    Connection connection;
    User user;

    @Before
    public void makeConnection() throws Exception {
        connection = new Connection();
        connection.init();
    }

    @After
    public void closeConnection() {
        connection.tearDown();
    }

    @Given("^we are user$")
    public void we_are_user() throws Throwable {
        user = new User(
            loadproperty.loadProperties().getProperty("email"),
            loadproperty.loadProperties().getProperty("password"),
            loadproperty.loadProperties().getProperty("wrongEmail"),
            loadproperty.loadProperties().getProperty("wrongPassword"),
            loadproperty.loadProperties().getProperty("address")
        );
    }

    @And("^we enter to SpidApp$")
    public void we_enter_to_SpidApp() throws Throwable {
        Welcome wel = new Welcome(connection.getDriver());
        wel.enter();
    }

    @When("^we make login with wrong email and password$")
    public void we_make_login_with_wrong_email_and_password() throws Throwable {
    }

    @Then("^the login is not successfull$")
    public void the_login_is_not_successfull() throws Throwable {
        /*
        Login login = new Login(driver);
        login.validateLogin();
        */
    }
}
