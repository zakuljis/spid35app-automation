package steptsDefinitions.Cart;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.runner.RunWith;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import models.User;
import pages.*;
import utilities.Connection;
import utilities.Product;
import utilities.PropertyLoader;

@RunWith(Cucumber.class)
public final class CartStepts {
    User user;
    Connection connection;
    Login loginPage;
    Home homePage;

    @Before
    public void makeConnection() throws Exception {
        connection = new Connection();
        connection.init();
    }

    @After
    public void closeConnection() {
        connection.tearDown();
    }

    @Given("we are user")
    public void we_are_user() throws Throwable {
        user = new User(
            PropertyLoader.getProperty("email"),
            PropertyLoader.getProperty("password"),
            PropertyLoader.getProperty("wrongEmail"),
            PropertyLoader.getProperty("wrongPassword"),
            null
        );
    }

    @And("we enter to SpidApp")
    public void we_enter_to_SpidApp() throws Throwable {
        Welcome welcome = new Welcome(connection.getDriver());
        welcome.enter();
    }

    @And("we allow the permissions")
    public void we_allow_the_permissions() throws Exception {
        loginPage = new Login(connection);
        loginPage.allowPermissions();
    }

    @And("we are in Colombia")
    public void we_are_from_Colombia() throws Throwable {
        loginPage.setCountry("Colombia");
        HashMap<String, String> propByCountry = PropertyLoader.getProperty("address");
        user.setAddress(propByCountry.get("Colombia"));
    }

    @And("we log in")
    public void we_log_in() throws Exception {
        loginPage.logIn(user);
        loginPage.declineTouchId();
    }

    @And("we set a valid address")
    public void we_set_a_valid_address() throws Exception {
        homePage = Home.createInstance(connection);
        homePage.setAddress(user.getAddress());
    }

    @When("we add only 1 unit of an item to the cart from {word} page")
    public void we_add_only_1_unit_of_an_item_to_the_cart(String section) throws Exception  {
        Product product = homePage.getProduct(section);
        int initCartSize = homePage.getCartSize();
        product.Add(3);
        product.Remove(3);
        product.Add();
        assertTrue(homePage.isCartSize(initCartSize + 3 - 3 + 1));
    }

    @Then("the remove icon was a thrash can")
    public void the_remove_icon_was_a_thrash_can() {
        System.out.println("3");
    }
}
