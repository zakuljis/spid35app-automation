Feature: Modify the cart
    As a user
    I want to add/remove items from my cart

    Background:
        Given we are user
        * we enter to SpidApp
        * we allow the permissions
        * we are in Colombia
        * we log in
        * we set a valid address
    
    Scenario Outline: Add only 1 unit of an item
        When we add only 1 unit of an item to the cart from <section> page
        Then the remove icon was a thrash can

        Examples:
            | section  |
            | Home     |
            | PLP      |
            | PDP      |

    Scenario Outline: Icons change
        Given an item that is not in the cart
        When we add more than 1 unit of an item to the cart from <section> page
        Then the remove icon was a minus sign

        Examples:
            | section  |
            | Home     |
            | PLP      |
            | PDP      |
    
    Scenario Outline: Cart update
        When we add and remove an item from <section> page
        Then the cart was updated successfully

        Examples:
            | section  |
            | Home     |
            | PLP      |
            | PDP      |
