# ATENCION: Este test, cuando lo hice, sólo funcionaba en dev
Feature: Create a new account in the SpidApp
    As a customer
    I want to register in the SpidApp

    Background:
        Given we are customer
        * we enter to SpidApp
        * we allow the permissions

    Scenario Outline: List of identity documents
        Given we are in <country>
        When we open the register form
        Then the list of document types shown belongs to <country>

        Examples:
            | country   |
            | Argentina |
            | Brasil    |
            | Colombia  |
            | Perú      |
    
    Scenario Outline: Size of identity documents
        Given we are in <country>
        When we open the register form
        And we fill it all with valid data except the identity document
        Then only the right-sized id numbers for each of the <country>'s document types let us to continue

        Examples:
            | country   |
            | Argentina |
            | Brasil    |
            | Colombia  |
            | Perú      |

    Scenario Outline: Terms and conditions
        Given we are in <country>
        When we open the register form
        And we make register with right values
        Then we can see the terms and conditions

        Examples:
            | country   |
            | Argentina |
            | Brasil    |
            | Colombia  |
            | Perú      |

    Scenario: Perú privacy policy
        Given we are in Perú
        When we open the register form
        And we make register with right values
        Then we can see SpidApp's privacy policy
    
    Scenario Outline: Do correct register in the app
        Given we are in <country>
        When we open the register form
        And we make register with right values
        And we create valid password
        Then the register is successfull

        Examples:
            | country   |
            | Argentina |
            | Brasil    |
            | Colombia  |
            | Perú      |
