Feature: Set an address in the SpidApp
    As a customer
    I want to set an address in the SpidApp

    Background:
        Given we are customer
        * we enter to SpidApp
        * we are in Colombia
        * we log in as a guest
        * we allow the permissions

    Scenario Outline: Enter a address
        When we search a <status> address
        Then the app <result> find that address

        Examples:
            | status  | result   |
            | valid   | should   |
            | invalid | shouldnt |
    
    Scenario Outline: Enter a valid address
        When we set a valid address <coverage> range
        Then the app <result> allow us to search products
    
        Examples:
            | coverage | result   |
            | within   | should   |
            | out-of   | shouldnt |

    Scenario: Closed stores
        When we set a coveraged address out of hours
        Then the app should tell us that the store is closed
