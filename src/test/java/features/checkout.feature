Feature: Make a checkout in the SpidApp
    As a user
    I want to checkout in the SpidApp

    Background:
        Given we are customer
        * we enter to SpidApp
        * we allow the permissions
    
    Scenario Outline: Check subtotal
        Given we are in <country>
        * we log in
        * we use or set a valid address
        When we go to cart detail
        Then the subtotal and discounts will be right
    
        Examples:
            | country   |
            | Argentina |
            | Brasil    |
            | Colombia  |
            | Perú      |

    Scenario Outline: Do a successful checkout 
        Given we are in <country>
        * we log in
        * we use or set a valid address
        * we add an item to the cart
        * we go to the checkout
        * we select <method> payment method 
        * we <state> fill the payment form with a valid credit card in <country>
        When we submit the payment form
        Then the checkout will be successful
            
        Examples:
            | country  | method                        | state    |
            | Colombia | "Paga en la entrega"          | shouldnt |
            | Colombia | "Paga con tarjeta de crédito" | should   |
            | Brasil   | "Pagar na entrega"            | shouldnt |
            | Brasil   | "Pagar com cartão de crédito" | should   |
