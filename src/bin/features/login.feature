Feature: Make login in the SpidApp
  As a user
  I want to make login in the SpidApp

  Background:
    Given we are user
    And we enter to SpidApp

  Scenario: Do correct login in the app
    When we make login with email and password
    Then the login is successfull
    And we logout

  Scenario: Do incorrect login in the app
    When we make login with wrong email and password
    Then the login is not successfull
