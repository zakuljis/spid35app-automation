# Spidapp Automation
Estos son los los *unit tests* de la aplicación *Spid Regional* de Cencosud.
## Cómo instalar
Primeramente, es necesario instalar *Java*; por un problema con una dependencia de [appium/java-client](https://github.com/appium/java-client) las últimas versiones de Java no son compatibles. Un rango seguro de uso para este framework contempla desde la versión 1.8.0 hasta la [15.0.2](https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html), la cual se recomienda descargar.

Luego, es necesario *Apache maven* como gestor de proyecto, el archivo binario se puede descargar [aquí](https://maven.apache.org/download.cgi). Además de seguir las [instrucciones proporcionadas en su wiki](https://maven.apache.org/install.html), si se desea instalar en Windows, es recomendable tener conocimiento sobre las *Environment variables*.

Ahora hay que instalar el servidor de appium, se recomienda [appium/appium-desktop](https://github.com/appium/appium-desktop/releases).
Adicional a esto, si su deseo es extender el *testing* y no simplemente ejecutarlo, es recomendable instalar la herramienta de desarrollo [appium/appium-inspector](https://github.com/appium/appium-inspector).

Finalmente resta tener un dispositivo en el cual se deseen ejecutar las pruebas.
Para Android, se recomiendan los emuladores de [Android studio](https://developer.android.com/studio); una vez instalado puede crear emuladores de android de la [siguiente forma](https://developer.android.com/studio/run/managing-avds).
En iOs puede usar los *simulators* de xCode, para ello puede seguir las [instrucciones proporcionadas por Apple](https://developer.apple.com/documentation/xcode/running-your-app-in-the-simulator-or-on-a-device).

## Ejecutar las pruebas
Cabe recalcar que las pruebas deben ser realizadas en la rama dev.

Antes de nada, es necesario configurar ciertos parámetros. En el archivo *params.yml*, en Android, es necesario fijarse en estos dos valores:
```
deviceNameAND: emulator-5554
avd: Pixel_4_API_30
```
`deviceNameAnd` corresponde al dispositivo que se está usando, este parámetro es importante si usted <ins>posee más de 1 emulador</ins> instalado, en el caso contrario, seguramente el valor correcto sea `emulator-5554`. Para ver todos los emuladores que tiene instalado, puede ejecutar el siguiente comando en su terminal:
```
adb devices
```
`avd` corresponde al nombre del dispositivo que está usando, en el caso de Android studio, puede ver este nombre en la *propierty* `AvdId` al clickear sobre su emulador en la pestaña de selección de emuladores.

En iOS, tendrá que revisar los siguientes valores:
```
deviceNameIOS: iPhone 12 Pro
platformVersionIOS: "15.2"
appPathIOS: "/Users/<user>/Library/Developer/Xcode/DerivedData/Spid-byzgmwaewmcgxtgcuawmzsobpnrl/Build/Products/Debug-iphonesimulator/Spid.app"
```
`deviceNameIOS` corresponde al nombre del dispositivo, si está usando xCode, esto corresponde al nombre del simulador que está usando.

`platformVersionIOS` es la versión de iOS que tiene el simulador, esta información también es entregada por xCode. Es importante que este valor se encuentre entre comillas. 

`appPathIOS` indica la ruta en la que se encuentra el archivo `.app`, si se está usando un simulador, o `.ipa`, si se está usando un dispositivo real. Este archivo sirve para reinstalar la aplicación cada vez que se ejecute el test.

A lo largo del archivo *params.yml* verá una serie de parámetros más, es necesario que, de cambiar algún texto en la aplicación, se revisen estos, ya que cualquier discordancia generará un fallo en las pruebas.

Una vez terminada la configuración, se puede elegir qué feature ejecutar desde el archivo `src\test\java\testRunner\runTest.java`
```
features = "src/test/java/features/<feature>.feature",
    plugin = {
        "pretty",
        "html:target/cucumber",
        "json:target/cucumber.json"
    },
    glue = "steptsDefinitions.<stepts>",
    stepNotifications = true
```
Donde *\<feature\>* representa el test que se desea ejecutar (register, login, availability, cart, checkout), disponibles en `src\test\java\features`. Y \<stepts\> a la definción de las funciones a utilizar, disponibles en `src\test\java\steptsDefinitions`, bastará con poner en este campo la misma *feature* con la inicial en mayúsucla, como se puede ver a continuación:
```
features = "src/test/java/features/register.feature",
    plugin = {
        "pretty",
        "html:target/cucumber",
        "json:target/cucumber.json"
    },
    glue = "steptsDefinitions.Register",
    stepNotifications = true
```

Las pruebas se pueden ejecutar con el siguiente comando:
```
mvn test
```

Es importante recalcar que las *Failures* indican que el test se ha ejecutado correctamente y ha encontrado un comportamiento incorrecto en la aplicación, mientras que los *Errors* indican que el test tuvo un error, esto no necesariamente implica que la aplicación tenga un mal comportamiento.
## To Do
### Android
- [x] Register
- [ ] Login<br/>
    No se llegó a implementar este test.
- [x] Availability
- [x] Cart<br/>
    El escenario "Icons change" no funciona del todo en la PDP puesto que la aplicación no proporcionaba la información necesaria para determinar si los íconos cambiaban o no.
- [ ] Checkout<br/>
    Cuando se programó el test había un bug que impedía finalizar la compra, por lo que, después de presionar el botón para realizar el pago, no se tiene certeza si el test funciona o no.
### iOS
- [x] Register
- [ ] Login<br/>
    No se llegó a implementar este test.
- [ ] Availability<br/>
    Cuando se programó el test había un bug que arrojaba resultados incorrectos a la búsqueda de direcciones, por lo que el test no pudo avanzar luego de ese punto.
- [ ] Cart<br/>
    No se llegó a implementar este test
- [ ] Checkout<br/>
    No se llegó a implementar este test
### Observaciones
Cuando comencé a programar los tests adquerí la mala práctica de separar todas las Stepts de cada una de las features en su propia carpeta, eso generó mucho código duplicado (Como por ejemplo la función "we enter to SpidApp", que está escrita en todos los archivos de Stepts), en el futuro se debería refactorizar esto y añadir archivos de código general. Los últimos tests que programé fueron *checkout* (En Android) y *register* (En iOS), por lo que estos tienen las versiones más actualizadas de estas funciones.